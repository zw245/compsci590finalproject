# IMDB

## Installation

1. Install Docker 
2. `docker pull postgres`
3. `docker pull baz55w/imdb_project`

## Start postgres (on master branch)
Run `docker compose up`
Postgres port: 5500

## Start backend (on master branch)
Run `sbt run`
Backend port: 8080
